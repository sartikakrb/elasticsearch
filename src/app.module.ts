import { Module } from '@nestjs/common';
import { ElasticSearch } from './config/elastic';
import { ConversationService } from './module/conversation/conversation.service';
import { ConversationController } from './module/conversation/conversation.controller';
import { ArangoToElastic } from './command/arango-to-elastic';
import { ConsoleModule } from '@squareboat/nest-console';
import { Arango } from './config/arango';
import { ElasticService } from './common/elastic';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConsoleModule,
    ConfigModule.forRoot()],
  controllers: [ConversationController],
  providers: [ElasticSearch, ConversationService, ArangoToElastic, Arango, ElasticService],
})
export class AppModule {}
