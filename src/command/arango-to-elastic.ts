import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Arango } from '../config/arango';
import { Command, _cli } from '@squareboat/nest-console';
import { ElasticService } from 'src/common/elastic';
//import { async_timer } from 'execution-time-decorators';

@Injectable()
export class ArangoToElastic {
  private readonly CONVERSATION_INDICES = 'conversations';

  constructor(
    private readonly arangodbService: Arango,
    private readonly elasticService: ElasticService,
  ) {}

  @Command('ingest:all-conversation-arango', {
    desc: 'Test Command',
    args: { name: { req: false } },
  })
  async handle() {
    Logger.log(`Start to Ingest All Conversation Data`);

    const exists = await this.elasticService.checkIndicesExist(this.CONVERSATION_INDICES);
    Logger.log(`Index found : ${exists}`);
    if(exists) {
      await this.elasticService.deleteIndices(this.CONVERSATION_INDICES);
    }
    const bodyMapping = {
      mappings: {
        properties: {
          id : { type: "text" },
          status : { type: "text" },
          createdAt : {
            type: "date",
            format: "yyyy-MM-dd HH:mm:ss"
          },
          transaction : {
            properties: {
              id : { type: "text" },
              state : { type: "text" },
            }
          },
          patient : {
            properties: {
              id : { type: "text" },
              fullName : { type: "text" },
            }
          },
          lastName : {
            properties: {
              content : { type: "text" },
              contentType : { type: "text" },
            }
          }
        }
      }
    };
    await this.elasticService.createIndices(this.CONVERSATION_INDICES, bodyMapping);

    await this.getAllConversation();

    Logger.log(`Finish to ingest all conversation data`);
  }

  async getAllConversation() {
    const totalConversations = await this.getConversationTotal();
    let totalProcessed = 0;
    let page = 0;
    const limit = 100;
    const allConversations : any = [];
    while(totalProcessed < totalConversations) {
      const offset = page > 0 ? (page - 1) * limit : 0;
      let filter = ` FILTER conv.deletedAt == NULL `;
      const pagination = ` LIMIT @offset, @limit`;
      const query = `FOR conv IN conversations
      ${filter}
      SORT conv.creaedAt ASC
      ${pagination}
      LET trans = (FOR trx in transactions
        FILTER trx.id == conv.transactionId
           LET patient = (FOR patient in patients
               FILTER patient.id == trx.patientId
               RETURN {id : patient.id, fullName : CONCAT(patient.patientFName, patient.patientFName && patient.patientLName ? ' ': '', patient.patientLName)}
               )
          return {transaction : merge({id : trx.id, state: trx.state}), patient : patient[0]})
      LET lastMsg = (FOR msg IN messages
        FILTER msg.conversationId == conv.id
        SORT msg.createdAt DESC
        RETURN {content : msg.content, contentType: msg.contentType}
        )
      LET localDateTime = DATE_ADD(conv.createdAt, 7, "hours")
      RETURN {id: conv.id, status: conv.status,
        createdAt: DATE_FORMAT(DATE_ISO8601(localDateTime), '%yyyy-%mm-%dd %hh:%mm:%ss') , transaction : trans[0].transaction, patient: trans[0].patient,
        lastMessage: lastMsg[0]}`;

      const convs = await this.arangodbService.clientRef.query({
        query: query,
        bindVars: {
          offset: offset,
          limit: limit,
        },
      });
      const result : any = await convs.map((x) => x);
      totalProcessed += result.length;
      page++;

      allConversations.push(result);

      const body = result.flatMap((doc : any) => [{ index: { _index: this.CONVERSATION_INDICES,  _id: doc.id } }, doc])
      await this.elasticService.insertBulk(body);

      Logger.log(`Fetching ${result.length} ${page}`);
   }
  }

  async getConversationTotal() {
    const query = `
      FOR doc IN conversations
      FILTER doc.deletedAt == NULL
      COLLECT WITH COUNT INTO length
      RETURN length
    `;
    const conversations = await this.arangodbService.clientRef.query({
      query,
      bindVars: {},
    });
    const result = await conversations.map((x) => x);

    if (result.length === 0) {
      throw new NotFoundException(`No conversation to be ingested`);
    }
    return result[0];
  }

}
