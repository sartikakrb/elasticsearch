import { Injectable } from '@nestjs/common';
import { ElasticSearch } from '../config/elastic';

@Injectable()
export class ElasticService {
    constructor(
        private readonly elasticSearch: ElasticSearch,
    ) {}

    async checkIndicesExist(indicesName : String) : Promise<Boolean> {
      return await this.elasticSearch.client.indices.exists({index: indicesName})
    }

    async deleteIndices(indicesName : String) : Promise<Boolean> {
      return await this.elasticSearch.client.indices.delete({ index: indicesName })
    }

    async createIndices(indicesName : String, body : any) : Promise<Boolean> {
        return await this.elasticSearch.client.indices.create({
            index: indicesName,
            body: body,
           });
    }

    async insertBulk( body : any) {
        return await this.elasticSearch.client.bulk({ refresh: true, body });
    }

    async updateRecord(indicesName : String, body : any) {
        await this.elasticSearch.client.index({
            index: indicesName,
            id: body.id,
            body: body,
        });
    }

    async searchData(indicesName: String, queryTerm: any, offset: number, limit: number) {
        return await this.elasticSearch.client.search({
            index: indicesName,
            from: offset,
            size: limit,
            body: queryTerm,
        }, {
            ignore: [404],
            maxRetries: 3
        })
    }

    async searchDataWithPaginaton(indicesName: String, queryTerm: any, offset: number, limit: number) {
        return await this.elasticSearch.client.search({
            index: indicesName,
            from: offset,
            size: limit,
            body: queryTerm,
        }, {
            ignore: [404],
            maxRetries: 3
        })
    }
}
