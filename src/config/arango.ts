import { Injectable } from '@nestjs/common';
import { Database } from 'arangojs';

@Injectable()
export class Arango {
  private readonly client: Database;

  constructor() {
    this.client = new Database({
      url: 'http://34.101.106.153:8529',
      databaseName: 'dev',
      auth: {
        username: 'dev',
        password: 'P@ssw0rd',
      },
      agentOptions: {
        rejectUnauthorized: false,
      },
    });
  }

  get clientRef(): Database {
    return this.client;
  }
}
