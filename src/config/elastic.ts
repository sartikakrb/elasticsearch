import { Injectable, HttpException } from '@nestjs/common';
import { Client } from 'elasticsearch';

@Injectable()
export class ElasticSearch {
  client: Client;
  constructor() {
     this.client = new Client({
      host: `http://${process.env.ELASTIC_USERNAME}:${process.env.ELASTIC_PASSWORD}@${process.env.ELASTIC_HOST}`,
    });
    this.client.ping({ requestTimeout: 3000 }).catch((err: any) => {
      throw new HttpException(
        {
          status: 'error',
          message: `Unable to reach Elasticsearch cluster ${err}`,
        },
        500,
      );
    });
  }

}
