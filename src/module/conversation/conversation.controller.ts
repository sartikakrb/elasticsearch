import { Controller, Get, Post, Query, Body, HttpException, HttpStatus } from '@nestjs/common';
import { ConversationOutput } from './conversation.model';
import { ConversationService } from './conversation.service';

@Controller('api/v1')
export class ConversationController {
  constructor(private readonly conversationService: ConversationService) {}

  @Get('/conversation/search')
  async searchByConversationId(
    @Query('conversationId') conversationId: string,
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('startDate') startDate: string,
    @Query('endDate') endDate: string,
  ): Promise<ConversationOutput> {
    if(!conversationId && (!startDate && !endDate)) {
      throw new HttpException(
        { error: 'must specified conversation id or date range' },
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
    return this.conversationService.searchConversationById(conversationId, offset, limit, startDate, endDate);
  }

  @Post('/conversation/save')
  async saveConversation(
    @Body('conversationId') conversationId: string,
  ): Promise<any> {
    return this.conversationService.saveConversation(conversationId);
  }
}
