
export class Conversation {
    id: string;
    status: string;
    createdAt: string;
    transaction: Transaction;
    patient: Patient;
    lastMessage: Message;
}

export class Transaction {
    id : string;
    state: string;
}

export class Patient {
    id: string;
    fullName: string;
}

export class Message {
    content: string;
    contentType: string;
}

export class ConversationMappedOutput {
   conversation : ConversationOutput;
}

export class ConversationOutput {
    data: any[];;
    //pageInfo : any;
}