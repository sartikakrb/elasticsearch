import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { ElasticService } from 'src/common/elastic';
import { Arango } from 'src/config/arango';
import { ConversationOutput } from './conversation.model';

@Injectable()
export class ConversationService {

  private readonly CONVERSATION_INDICES = 'conversations';
  constructor(
      private readonly elasticService: ElasticService,
      private readonly arangodbService: Arango,
  ) {}

  async searchConversationById(conversationId : string, offset: number, limit: number, startDate: string, endDate: string): Promise<ConversationOutput> {
    Logger.log(`Searching conversationId : ${conversationId}`);
    let queryConversationId = {};

    if(conversationId) {
        queryConversationId = {
          multi_match: {
          query: conversationId,
          fields: ["id"],
          type: "phrase_prefix"
        }
      }
    } else {
        queryConversationId = {match_all: {}}
      }

    const queryTerm = {
      query: {
        bool: {
          must: [
            queryConversationId,
            {
              range: {
                createdAt: {
                gte: startDate,
                lte: endDate,
                format: "yyyy-MM-dd"
                }
              }
            }
          ]
        }
      },
      sort: {
        createdAt: {
          order: "desc"
        }
      }
    }

    const response = await this.elasticService.searchDataWithPaginaton(this.CONVERSATION_INDICES, queryTerm, offset, limit);
    Logger.log(`Response of querying conversation id ${conversationId} ${JSON.stringify(response)}`);

    let mappedResponse : any = [];
    if(response.hits.total.value == 0) {
      mappedResponse = [];
    } else {
      const source = response.hits.hits.map(x => x._source);
      mappedResponse = source;
    }
    return {
      data : mappedResponse
    };
  }

  async saveConversation(conversationId : string) : Promise<any>{
    Logger.log(`Saving new record to elastic for conversation id : ${conversationId}`);
    const conversation = await this.getConversationById(conversationId);
    return await this.elasticService.updateRecord(this.CONVERSATION_INDICES, conversation);
  }

  async getConversationById(conversationId : string) : Promise<any>{
    const query = `
    FOR conv IN conversations
      FILTER conv.id == @conversationId
      SORT conv.creaedAt ASC
      LET trans = (FOR trx in transactions
        FILTER trx.id == conv.transactionId
           LET patient = (FOR patient in patients
               FILTER patient.id == trx.patientId
               RETURN {id : patient.id, fullName : CONCAT(patient.patientFName, patient.patientFName && patient.patientLName ? ' ': '', patient.patientLName)}
               )
          return {transaction : merge({id : trx.id, state: trx.state}), patient : patient[0]})
      LET lastMsg = (FOR msg IN messages
        FILTER msg.conversationId == conv.id
        SORT msg.createdAt DESC
        RETURN {content : msg.content, contentType: msg.contentType}
        )
      LET localDateTime = DATE_ADD(conv.createdAt, 7, "hours")
      RETURN {id: conv.id, status: conv.status,
        createdAt: DATE_FORMAT(DATE_ISO8601(localDateTime), '%yyyy-%mm-%dd %hh:%mm:%ss'), transaction : trans[0].transaction, patient: trans[0].patient,
        lastMessage: lastMsg[0]}`;

    const conversationResultSet = await this.arangodbService.clientRef.query({
        query: query,
        bindVars: {
          conversationId : conversationId
        },
    });
    const conversation : any = await conversationResultSet.map((x) => x);

    if (conversation.length === 0) {
      throw new NotFoundException(`conversation id :  ${conversationId} not exist`);
    }

    return conversation[0];
  }


}
